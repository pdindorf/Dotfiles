
# Destination aliases:
alias tex='cd $HOME/Documents/tex/'
alias proj='cd $HOME/Documents/Projects'
alias tmux='tmux -2'
alias wiki='vim $HOME/vimwiki/index.wiki'
alias gwiki='cd $HOME/vimwiki/'
alias gs='cd $HOME/.vim/snippets'
alias pdf='mupdf'
